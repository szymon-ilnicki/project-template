/**
 * @description       : utility class for enabling and disabling triggers
 * @author            :  | 04.01.2021
 **/
global class UTIL_Triggers {
    private static final String ENABLED = 'ENABLED';
    private static final String DISABLED = 'DISABLED';
    
    private static Map<String, String> handlerStates = new Map<String, String>();
    private static Map<String, String> triggerStates = new Map<String, String>();

    public static User currentUser;

    global static Boolean isHandlerEnabled(String handler) {
        // If not explicitly stated as disabled, assume its enabled
        if(!handlerStates.containsKey(handler)) {
            return TRUE;
        }
        return (handlerStates.get(handler) == ENABLED) ? TRUE : FALSE;
    }

    global static void disableHandler(String handler) {
        handlerStates.put(handler, DISABLED);
    }

    global static void enableHandler(String handler) {
        handlerStates.put(handler, ENABLED);
    }

    global static Boolean isTriggerEnabled(String triggerName) {
        if(currentUser == NULL) {
            currentUser = [SELECT Disable_All_Triggers__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        }
        if(currentUser.Disable_All_Triggers__c == TRUE) {
            return FALSE;
        }
        // If not explicitly stated as disabled, assume its enabled
        if(!triggerStates.containsKey(triggerName)) {
            return TRUE;
        }
        return (triggerStates.get(triggerName) == ENABLED) ? TRUE : FALSE;
    }

    global static void disableTrigger(String triggerName) {
        triggerStates.put(triggerName, DISABLED);
    }

    global static void enableTrigger(String triggerName) {
        triggerStates.put(triggerName, ENABLED);
    }
}