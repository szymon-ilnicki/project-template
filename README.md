## About the project
--- Salesforce project

Repository in SFDX source format

## Manifest Files
1. *package.xml* - main __full__ mainfest file - describing whole Salesforce project

## Deployment Commands Cheatsheet
1. Retrieving full code from environment

`sfdx force:source:retrieve --manifest manifest/package.xml -u <env>`

## Enxoo Commands Cheatsheet
1. Retrieve products from org

`sfdx enxoo:cpq:prd:retrieve -p '*ALL' -d productCatalog -u <env> -b`

2. Import products to an org

`sfdx enxoo:cpq:prd:import -p '*ALL' -d productCatalog -u <env> -b`

3. Retrieve settings from org

`sfdx enxoo:cpq:settings:retrieve -u <env> -d productCatalog`

4. Deploying settings to org

`sfdx enxoo:cpq:settings:import -u <env> -d productCatalog`

## SFDX Basic Commands Cheatsheet

1. List orgs where your user is authorised
`sfdx force:org:list`

2. Authorise to an org using web login flow

`sfdx force:auth:web:login -r https://login.salesforce.com/ -a <alias_to_be_set>`

3. Setting a default username

`sfdx force:config:set defaultusername=<org_alias>`

## Configure Your Salesforce DX Project

The `sfdx-project.json` file contains useful configuration information for your project. See [Salesforce DX Project Configuration](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_ws_config.htm) in the _Salesforce DX Developer Guide_ for details about this file.

## Read All About It
- [Project Template Recipe Documentation](https://enxooteam.atlassian.net/wiki/spaces/ECPQKB/pages/2755395958/Recipe+-+CI+CD+on+Bitbucket)
- [Salesforce Extensions Documentation](https://developer.salesforce.com/tools/vscode/)
- [Salesforce CLI Setup Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_intro.htm)
- [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_intro.htm)
- [Salesforce CLI Command Reference](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference.htm)