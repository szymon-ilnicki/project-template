#!/bin/bash

SFDX_CLIENT_ID = $1
SFDX_USER = $2
sfdx force:auth:jwt:grant --instanceurl https://test.salesforce.com --clientid $SFDX_CLIENT_ID --jwtkeyfile pipelines/server.key --username $SFDX_USER --setalias sit

# Package Ids
enxCPQID = $3
enxB2BID = $4

echo 'Installing Enxoo Commerce (enxCPQ), please wait...'
sfdx force:package:install --package $enxCPQID  -u sit -w 15
echo 'Installing  Enxoo for Communications (enxB2B), please wait...'
sfdx force:package:install --package $enxB2BID  -u sit -w 15

echo 'Installing  Test Runner, please wait...'
sfdx force:package:install --package 04t4I000000lFB8QAM  -u sit -w 15

echo 'Packages successfully installed:'
sfdx force:package:installed:list

add_permsets="y"

if [[ $add_permsets =~ ^[yY]$ ]]; then
	echo 'Setting CPQ & B2B permsets...'
	sfdx force:user:permset:assign --permsetname CPQ_Administrator -u sit
	sfdx force:user:permset:assign --permsetname CPQ_User -u sit
	sfdx force:user:permset:assign --permsetname B2B_Administrator -u sit
	sfdx force:user:permset:assign --permsetname B2B_User -u sit
	sfdx force:user:permset:assign --permsetname B2B_OM_Administrator -u sit
	sfdx force:user:permset:assign --permsetname B2B_OM_User -u sit
	sfdx force:user:permset:assign --permsetname B2B_SA_User -u sit
fi

echo 'Dev org initialized successfully!'